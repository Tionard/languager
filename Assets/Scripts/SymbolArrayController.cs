﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SymbolArrayController : MonoBehaviour
{

    public GameObject symbolPrefab;
    [SerializeField]
    public int arraySize;
    public Slider slider;
    public TextMeshProUGUI elementsAmountText;


    void Start()
    {
        addToArray(arraySize);
        slider.value = arraySize;
        //elementsAmountText.text = ": " + slider.value;
        elementsAmountText.SetText(": " + slider.value);
    }

    public void addToArray(int amount)
    {
        for (int i = 0; i < Mathf.Abs(amount); i++)
        {
            var child = Instantiate(symbolPrefab, this.transform);
            child.SetActive(true);
        }
    }

    public void removeFromArray(int amount)
    {
        for (int i = 0; i < Mathf.Abs(amount); i++)
        {
            Destroy(this.transform.GetChild(this.transform.childCount - 1-i).gameObject);
        }
    }

    public void ChangeArraySize()
    {
        int difference = (int)slider.value - arraySize;
        if (difference > 0)
        {
            addToArray(difference);
        }
        else if (difference < 0)
        {
            removeFromArray(Mathf.Abs(difference));
        }
        arraySize = (int)slider.value;
        elementsAmountText.SetText(": " + slider.value);
    }

    public void RandomizeSymbols()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<RandomizeSymbol>().Randomize();
        }
    }
}

