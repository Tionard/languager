﻿using UnityEngine;
using UnityEngine.UI;

public class RandomizeSymbol : MonoBehaviour
{
    public GameObject[] panels;

    public Texture2D textureTopRight;
    public Texture2D textureBotRight;
    public Texture2D textureTopCenter;
    public Texture2D textureBotCenter;
    private Sprite[] spritesTop, spritesBot;

    private Sprite topSymbol, botSymbol;

    public void Randomize(){

        bool centering = (Random.value < 0.5);

        if(centering)
        {
            spritesTop = Resources.LoadAll<Sprite>(textureTopCenter.name);
            spritesBot = Resources.LoadAll<Sprite>(textureBotCenter.name);
        }
        else
        {
            spritesTop = Resources.LoadAll<Sprite>(textureTopRight.name);
            spritesBot = Resources.LoadAll<Sprite>(textureBotRight.name);
        }


        
        Sprite topSprite = (Sprite) spritesTop.GetValue(Random.Range(0,spritesTop.Length-1));
        Sprite botSprite;
        
        bool mirrorBotY = (Random.value < 0.5);
        bool mirrorX = (Random.value < 0.5);
        bool mirrorY = (Random.value < 0.5);


        float scaleBotX, scaleBotY, scaleTopX, scaleTopY;
        scaleBotX = scaleBotY = scaleTopX = scaleTopY = 1;

        
        if(mirrorBotY)
        {
            botSprite = (Sprite) spritesTop.GetValue(Random.Range(0,spritesTop.Length-1));
            scaleBotY = -1;
        }
        else
        {
            botSprite = (Sprite) spritesBot.GetValue(Random.Range(0,spritesBot.Length-1));
            scaleBotY = 1;
        }

         if(mirrorX)
            scaleTopX = scaleBotX = -1;
        else
            scaleTopX = scaleBotX = 1;

        panels[0].GetComponent<Image>().sprite = topSprite;
        panels[1].GetComponent<Image>().sprite = botSprite;

        panels[0].transform.localScale = new Vector3(scaleTopX, scaleTopY, 1);
        panels[1].transform.localScale = new Vector3(scaleBotX, scaleBotY, 1);
    

        
        /*
        foreach(GameObject g in panels){
            g.GetComponent<Image>().sprite = (Sprite) spritesTop.GetValue(Random.Range(0,spritesTop.Length-1));
        }
        */
    }
    // Start is called before the first frame update

    void Start(){
        Randomize();
    }


}
