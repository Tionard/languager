﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SymbolArrayControler : MonoBehaviour
{

    public GameObject symbolPrefab;
    [SerializeField]
    public int arraySize;


    void Start()
    {
        generateNewArray();
    }

    public void generateNewArray()
    {
        for (int i = 0; i < Mathf.Abs(arraySize); i++)
        {
            var child = Instantiate(symbolPrefab, this.transform);
            child.SetActive(true);
        }
    }

    public void RandomizeSymbols()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<RandomizeSymbol>().Randomize();
        }
    }
}
