# Languager (Unique Symbol Generator)

## Overview
Unique Symbols Generator is a small, fun project built in Unity. It generates unique symbols (letters/characters) for your made-up languages by combining 2 to 4 images "tiles" together like a puzzle.

## Features
- **Expandable "tiles" library:** It has default library of images "tiles" to create unique characters from.
- **Dynamic Compositions:** Use 2 to 4 "tiles", mirror or rotate them to give distinct look to your characters.

## About
This project was created purely for fun as an exploration into image manipulation and Unity�s creative potential. It serves as a playful experiment in generating unique language symbols/letters.

---

Feel free to explore, modify, and have fun with this project!